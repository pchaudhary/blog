<?php
require_once "config.php";

if (count($_POST))
{
    if ($_POST['title'] == '')
    {
        $error = "Title should not be empty";
    } else
    {
        $query = "UPDATE `blogs` SET `title` = '{$_POST['title']}', `content` = '{$_POST['content']}' WHERE `id` = {$_GET['id']}";

        $result = $connection->query($query);

        if ($result)
        {
            $newId = $connection->insert_id;
        } else
        {
            echo $connection->error;
        }
    }
}



if(isset($error)) {
    $title = $_POST['title'];
    $content = $_POST['content'];
} else {
    $query = "SELECT * FROM `blogs` where `id` =" . $_GET['id'];

    $fetchResult = $connection->query($query);
    $blog = $fetchResult->fetch_assoc();

    $title = $blog['title'];
    $content = $blog['content'];
}
require "header.php";

?>
<h1>Edit blog:</h1>
<?php
if (isset($error)) {
    echo "<span class=\"error\">" . $error . "</span>";
}

if (isset($result) && $result) { ?>
    <span style="background-color: green;">Your blog has been updated successfully.</span>
<?php } ?>

<form method="post">
    <input id="abcd" type="text" name="title" placeholder="Enter title of blog" value="<?= $title ?>" />
    <textarea type="text" name="content" placeholder="Enter content of blog here"><?= $content ?></textarea>
    <br>
    <br>
    <button type="submit" value="Create">Update</button>
</form>
</body>
</html>